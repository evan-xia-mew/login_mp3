#include "traymenu.h"
#include "alldialog.h"

TrayMenu::TrayMenu()
{
    initActions();
    connect(m_pShowAction,SIGNAL(triggered(bool)),this,SIGNAL(sigshow()));
    connect(m_pAboutAction,SIGNAL(triggered(bool)),this,SIGNAL(sigabout()));
    connect(m_pQuitAction,SIGNAL(triggered(bool)),this,SIGNAL(sigclow()));
    connect(m_pSmallAction,SIGNAL(triggered(bool)),this,SIGNAL(sigsmall()));

}
TrayMenu::~TrayMenu()
{
}

void TrayMenu::initActions()
{
    m_pShowAction = new QAction(QIcon(":/img/1.jpg"),"全屏显示");
    m_pSmallAction = new QAction(QIcon(":/img/8.jpg"),"窗口最小化");
    m_pAboutAction = new QAction(QIcon(":/img/2.jpg"),"关于作者");
    m_pQuitAction = new QAction(QIcon(":/img/6.jpg"),"退出界面");


    addAction(m_pShowAction);
    addAction(m_pSmallAction);
    addAction(m_pAboutAction);
    addSeparator();
    addAction(m_pQuitAction);
}
