#ifndef ALLDIALOG_H
#define ALLDIALOG_H

#include <QDialog>
#include "dialog.h"
#include "statusdialog.h"
#include "titledialog.h"
#include <QPushButton>
#include <QMouseEvent>
#include <QPoint>

#include <QSystemTrayIcon>
#include "traymenu.h"

namespace Ui {
class allDialog;
}

class allDialog : public QDialog
{
    Q_OBJECT

public:
    explicit allDialog(QWidget *parent = nullptr);
    ~allDialog();

public:
    int getflag(){
        return flag;
    }
    int changeflag(int x){
        flag=x;
        return 0;
    }

protected:

    void mouseMoveEvent(QMouseEvent *e)override;//鼠标移动
    void mousePressEvent(QMouseEvent *e)override;//鼠标按下移动
    void mouseReleaseEvent(QMouseEvent *e)override;//鼠标松开


public slots:
    void getslot(int);

    void showWindow();
    void smallWindow();
    void closeWindow();
    void aboutWindow();


private:
    Ui::allDialog *ui;
    titleDialog *m_pTitleBar;
    statusDialog *m_pStatusBar;
    Dialog *m_pCenterWidget;

    QSystemTrayIcon *m_pSystemTrayIcon;
    TrayMenu *m_pTrayMenu;

    int flag;

    QPoint m_point;
    bool m_bFlag;
};

#endif // ALLDIALOG_H
