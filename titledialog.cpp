#include "titledialog.h"
#include "ui_titledialog.h"
#include <QDebug>


titleDialog::titleDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::titleDialog)
{
    ui->setupUi(this);

    connect(ui->pushButton,&QPushButton::clicked,this,&titleDialog::butclose);
    connect(ui->pushButton_2,&QPushButton::clicked,this,&titleDialog::butmin);
    connect(ui->pushButton_3,&QPushButton::clicked,this,&titleDialog::butmax);

}

titleDialog::~titleDialog()
{
    delete ui;
}

void titleDialog::butclose()
{
    qDebug()<<"title:close(1)";
    emit sendmsg(1);
}

void titleDialog::butmin()
{
    qDebug()<<"title:small(2)";
    emit sendmsg(2);
}

void titleDialog::butmax()
{
    qDebug()<<"title:max(3)";
    emit sendmsg(3);
}

