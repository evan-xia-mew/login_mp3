#include "alldialog.h"
#include "ui_alldialog.h"
#include <QIcon>
#include <QtDebug>
#include <windows.h>

allDialog::allDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::allDialog)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint);
    this->setWindowTitle("登录界面");
    this->setWindowIcon(QIcon(":/img/1.jpg"));

    m_pTitleBar = new titleDialog;
    m_pStatusBar = new statusDialog;
    m_pCenterWidget = new Dialog;

    changeflag(false);
    ui->horizontalLayout_7->addWidget(m_pTitleBar);
    ui->horizontalLayout_8->addWidget(m_pCenterWidget);
    ui->horizontalLayout_9->addWidget(m_pStatusBar);

    m_pSystemTrayIcon =new QSystemTrayIcon(this);
    m_pTrayMenu =new TrayMenu;
    m_pSystemTrayIcon->setContextMenu(m_pTrayMenu);

    m_pSystemTrayIcon->setToolTip("天天音乐");
    m_pSystemTrayIcon->setIcon(QIcon(":/img/1.jpg"));
    m_pSystemTrayIcon->show();

    connect(m_pTitleBar,&titleDialog::sendmsg,this,&allDialog::getslot);
    connect(m_pTitleBar,&titleDialog::sendmsg,this,&allDialog::getslot);
    connect(m_pTitleBar,&titleDialog::sendmsg,this,&allDialog::getslot);
    connect(m_pCenterWidget,&Dialog::sendmsg,this,&allDialog::getslot);


    connect(m_pTrayMenu, SIGNAL(sigshow()), this, SLOT(showWindow()));
    connect(m_pTrayMenu, SIGNAL(sigsmall()), this, SLOT(smallWindow()));
    connect(m_pTrayMenu, SIGNAL(sigclow()), this, SLOT(closeWindow()));
    connect(m_pTrayMenu, SIGNAL(sigabout()), this, SLOT(aboutWindow()));

    Sleep(2000);
}

allDialog::~allDialog()
{
    delete ui;
}

void allDialog::getslot(int x)
{
    switch(x){
    case 1:
        this->close();
        delete m_pTrayMenu;
        m_pTrayMenu=nullptr;
        delete m_pSystemTrayIcon;
        m_pSystemTrayIcon=nullptr;
        break;
    case 2:
        if(!getflag())
        {
            this->showMaximized();
            changeflag(true);
        }else{
            this->showNormal();
            changeflag(false);
        }
        break;
    case 3:
        this->showMinimized();
        break;
    default:
        qDebug() << "default......";
        break;
    }
}

void allDialog::mousePressEvent(QMouseEvent *e)
{
    Q_UNUSED(e);//去掉不用的宏
    if(e->button() == Qt::LeftButton)
    {
        m_bFlag=true;
        m_point =e->pos();
    }
}

void allDialog::mouseReleaseEvent(QMouseEvent *e)
{
    Q_UNUSED(e);//去掉不用的宏
    if(e->button() == Qt::LeftButton)
    {
        m_bFlag=false;
    }
}

void allDialog::mouseMoveEvent(QMouseEvent *e)
{
    Q_UNUSED(e);//去掉不用的宏
    if(e->buttons() & Qt::LeftButton)
    {
        move(e->pos()+pos()-m_point);
    }
}

void allDialog::showWindow()
{   
    this->showMaximized();
    changeflag(true);
}

void allDialog::smallWindow()
{
   this->showMinimized();
}
void allDialog::aboutWindow()
{
    QMessageBox::information(NULL,"关于","Copyright@夏启阳");
}

void allDialog::closeWindow()
{
    this->close();
}

