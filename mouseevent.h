#ifndef MOUSEEVENT_H
#define MOUSEEVENT_H

#include <QWidget>
#include <QMouseEvent>
#include <QPoint>

class mouseevent :public QWidget{
    Q_OBJECT

public:
    mouseevent(QWidget *parent = nullptr);
    ~mouseevent();

protected:

    void mouseMoveEvent(QMouseEvent *e)override;//鼠标移动
    void mousePressEvent(QMouseEvent *e)override;//鼠标按下移动
    void mouseReleaseEvent(QMouseEvent *e)override;//鼠标松开

private:
    QPoint m_point;
    bool m_bFlag;
};

#endif // MOUSEEVENT_H
