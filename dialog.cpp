#include "dialog.h"
#include "ui_dialog.h"
#include <QDebug>
#include <QAction>
#include "alldialog.h"
#include "Commonhelper.h"


#define HKEY_INI_QT "F:/qt_learn/online_class/login/config.ini"
#define HKEY_QSS_QT "F:/qt_learn/online_class/login/login.css"

int cnt=5;

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint);
    this->setWindowTitle("登录界面");
    this->setWindowIcon(QIcon(":/img/1.jpg"));
    m_FlagRememberPWD = false;
    //读取css配置文件
    CommonHelper::setStyle(HKEY_QSS_QT);

    //this->setStyleSheet(QString::fromUtf8("background-image: url(:/img/11.jpg);color: rgb(255, 255, 255);"));
    //m_username="admin";
    //m_pwd="admin";
    m_pConfig=new Config(HKEY_INI_QT);
    m_username=m_pConfig->readSetting("user","name").toString();
    //m_pConfig->writeSetting("user","password","123456");
    m_pwd=m_pConfig->readSetting("user","password").toString();

    m_RememberName =  m_pConfig->readSetting("historyUser","name").toString();
    m_RememberPWD =  m_pConfig->readSetting("historyUser","password").toString();

    qDebug() <<"ini:user:" << m_username;
    qDebug() << "ini:PWD"<< m_pwd;


    ui->le->setPlaceholderText("Username");
    ui->le->setAlignment(Qt::AlignCenter);
    QAction *ia = new QAction(ui->le);
    ia->setIcon(QIcon(":/img/user.png"));
    ui->le->addAction(ia,QLineEdit::LeadingPosition );
    ui->le2->setPlaceholderText("Password");
    QAction *ib = new QAction(ui->le2);
    ib->setIcon(QIcon(":/img/Password.png"));
    ui->le2->addAction(ib,QLineEdit::LeadingPosition );
    ui->le2->setEchoMode(QLineEdit::Password);
    ui->le2->setAlignment(Qt::AlignCenter);
    setWindowIcon(QIcon(":/img/2.jpg"));
    ui->le->setMaxLength(12);
    ui->le2->setMaxLength(6);

    ui->pb->setFlat(true);

    //connect(ui->pb,&QPushButton::clicked,this,&Dialog::on_pb_clicked);
    connect(ui->pb, SIGNAL(clicked(bool)), this, SLOT(on_pb_clicked()),Qt::UniqueConnection);
    //connect(ui->ck,&QPushButton::clicked,this,&Dialog::setpass);
    connect(ui->ck,SIGNAL(clicked(bool)),this,SLOT(setpass(bool)));

    connect(ui->le,SIGNAL(editingFinished()),this,SLOT(auto_set_PWD()));
    connect(ui->ck_rem,SIGNAL(stateChanged(int)),this,SLOT(slot_rember_PWD(int)));
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pb_clicked()
{
    cnt--;
    qDebug() << cnt;
    if(cnt>0){
        if(ui->le->text().isEmpty()){
            qmg = new QMessageBox(QMessageBox::Warning,QString("错误"),QString("账户名不能为空，请重新输入，你还有"+ QString::number(cnt,10)+" 次机会"));
            QTimer::singleShot(5000,qmg,SLOT(accept()));
            qmg->exec();
           // QMessageBox::warning(this, "Warning","账户名不能为空，请重新输入，你还有 "+ QString::number(cnt,10)+" 次机会");
            ui->le2->clear();
            return;
        }
        if(ui->le2->text().isEmpty()){
            qmg = new QMessageBox(QMessageBox::Warning,QString("错误"),QString("密码为空，请重新输入，你还有"+ QString::number(cnt,10)+" 次机会"));
            QTimer::singleShot(5000,qmg,SLOT(accept()));
            qmg->exec();
            return;
        }
        //if(!m_username.compare(le->text()))
        if(ui->le->text().trimmed()==m_username){
            if(ui->le2->text().trimmed()==m_pwd){
                emit sendmsg(1);

                if(m_FlagRememberPWD)
                {
                    m_pConfig->writeSetting("historyUser","name",ui->le->text());
                    m_pConfig->writeSetting("historyUser","password",ui->le2->text());
                }
                m_mp3_widget=new Widget;
                m_mp3_widget->show();

            }else{
                qmg = new QMessageBox(QMessageBox::Warning,QString("错误"),QString("密码错误，请重新输入，你还有"+ QString::number(cnt,10)+" 次机会"));
                QTimer::singleShot(5000,qmg,SLOT(accept()));
                qmg->exec();
                ui->le2->clear();
            }
        }else{
            qmg = new QMessageBox(QMessageBox::Warning,QString("错误"),QString("账号未注册，请重新输入，你还有"+ QString::number(cnt,10)+" 次机会"));
            QTimer::singleShot(5000,qmg,SLOT(accept()));
            qmg->exec();
            ui->le->clear();
            ui->le2->clear();
        }
    }else{
        emit sendmsg(1);
        //close();
    }

}

void Dialog::setpass(bool flag)
{
    if(!flag)
        ui->le2->setEchoMode(QLineEdit::Password);
    else
        ui->le2->setEchoMode(QLineEdit::Normal);
}


void Dialog::slot_rember_PWD(int index)
{
    if(index)
    {
        m_FlagRememberPWD = true;
    }
    else {
        m_FlagRememberPWD = false;
    }
}

void Dialog::auto_set_PWD()
{
    if(!m_RememberName.compare( ui->le->text()))
    {
        ui->le2->setText(m_RememberPWD);
    }
}
