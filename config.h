#ifndef CONFIG_H
#define CONFIG_H

#include <QVariant>
#include <QSettings>
//配置文件
class Config
{
public:
    Config(const QString &path);
    ~Config();
    void writeSetting(QString strNode,QString key,QVariant qvar);
    QVariant readSetting(QString strNode,QString key);
private:
    QString m_strConfigFilePath;
    QSettings *m_pSettings;
};

#endif // CONFIG_H
