#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QMediaPlayer>
#include <QFileDialog>
#include <QMediaPlaylist>
#include <QSystemTrayIcon>
#include "traymenumplay.h"
#include <QDir>
#include <QListWidgetItem>
#include <QTimer>

#include "mouseevent.h"
#include <QCloseEvent>


QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public mouseevent
{
    Q_OBJECT

public:
    Widget(mouseevent *parent = nullptr);
    ~Widget();

protected:
    virtual void closeEvent(QCloseEvent *event) override;

private slots:
    void onTimerOut();
    void on_open_but_3_clicked();
    void on_pre_but_3_clicked();
    void on_ply_but_3_clicked();
    void on_pasu_but_3_clicked();
    void on_next_but_3_clicked();
    void on_stop_but_3_clicked();
    void on_verticalSlider_sliderMoved(int position);
    void on_listWidget_3_itemDoubleClicked(QListWidgetItem *item);
    void on_bar_sliderMoved(int position);
    void on_bar_sliderPressed();
    void on_bar_sliderReleased();


    void on_closebt_clicked();
    void on_smbt_clicked();

    void showWindowmp3();
    void smallWindowmp3();
    void closeWindowmp3();
    void aboutWindowmp3();

    void playWindowmp3();
    void pauseWindowmp3();
    void nextWindowmp3();
    void preWindowmp3();


private:
    Ui::Widget *ui;
    QMediaPlayer *qplayer;
    QMediaPlaylist* qPlayerList;
    QString path = "E:";
    QStringList fileNames;

    QSystemTrayIcon *m_pSystemTrayIconmp3;
    TrayMenuMplay *m_pTrayMenump3;

    void initStatus(QMediaPlayer::MediaStatus status);
    // 槽函数 监听QMediaPlayer::mediaStatusChanged信号
};
#endif // WIDGET_H
