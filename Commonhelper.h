#ifndef COMMONHELPER_H
#define COMMONHELPER_H

#include <QApplication>
#include <QFile>
#include <QDebug>

class CommonHelper{
public:
    static void setStyle(const QString sPath){
        QFile file(sPath);
        if(file.open(QFile::ReadOnly)){
            qApp->setStyleSheet(file.readAll());
            file.close();
        }else{
            qDebug()<<"qss file load failed!";
        }
    }
};


#endif // COMMONHELPER_H
