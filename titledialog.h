#ifndef TITLEDIALOG_H
#define TITLEDIALOG_H

#include <QDialog>


namespace Ui {
class titleDialog;
}

class titleDialog : public QDialog
{
    Q_OBJECT

public:
    explicit titleDialog(QWidget *parent = nullptr);
    ~titleDialog();


signals:
    void sendmsg(int);

private slots:
    void butclose();
    void butmin();
    void butmax();

private:
    Ui::titleDialog *ui;
};

#endif // TITLEDIALOG_H
