#include "widget.h"
#include "ui_widget.h"
#include <QDialog>
#include "alldialog.h"
#include <QDebug>
#include <QBitmap>
#include <QPainter>
#include "Commonhelper.h"
#include <QMessageBox>


#define VOL 50 //默认音量

bool if_reload=false;
bool if_play=false;

QTimer* timer;
//设置进度条最大值
int maxValue = 1000;
int playstat=0;
int playlist=0;

Widget::Widget(mouseevent *parent)
    : mouseevent(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint);
    this->setWindowTitle("MP3界面");
    this->setWindowIcon(QIcon(":/img/2.jpg"));
    this->setWindowOpacity(0.8);

    ui->tBR->setPlaceholderText("很抱歉，暂无歌词");
    //读取css配置文件
    //CommonHelper::setStyle("F:/qt_learn/online_class/dialog/login/login.css");
    //重绘窗口
    QBitmap qwin(this->size());
    qwin.fill();
    QPainter p(&qwin);
    p.setPen(Qt::NoPen);
    p.setBrush(Qt::black);
    p.drawRoundedRect(qwin.rect(),20,20);
    setMask(qwin);

    m_pSystemTrayIconmp3 =new QSystemTrayIcon(this);
    m_pTrayMenump3 =new TrayMenuMplay;
    m_pSystemTrayIconmp3->setContextMenu(m_pTrayMenump3);

    m_pSystemTrayIconmp3->setToolTip("天天音乐");
    m_pSystemTrayIconmp3->setIcon(QIcon(":/img/4.jpg"));
    m_pSystemTrayIconmp3->show();


    ui->closebt->setFlat(true);
    ui->smbt->setFlat(true);
    ui->open_but_3->setFlat(true);
    ui->pre_but_3->setFlat(true);
    ui->ply_but_3->setFlat(true);
    ui->pasu_but_3->setFlat(true);
    ui->next_but_3->setFlat(true);
    ui->stop_but_3->setFlat(true);
    //connect(m_pTrayMenu,&TrayMenu::signalShowWindow,this,&Widget::showWindow);
    //connect(m_pTrayMenu,&TrayMenu::signalShowWindow,this,&Widget::closeWindow);
    connect(m_pTrayMenump3, SIGNAL(sigshowmp3()), this, SLOT(showWindowmp3()));
    connect(m_pTrayMenump3, SIGNAL(sigsmallmp3()), this, SLOT(smallWindowmp3()));
    connect(m_pTrayMenump3, SIGNAL(sigclowmp3()), this, SLOT(closeWindowmp3()));
    connect(m_pTrayMenump3, SIGNAL(sigaboutmp3()), this, SLOT(aboutWindowmp3()));

    connect(m_pTrayMenump3, SIGNAL(sigplaymp3()), this, SLOT(playWindowmp3()));
    connect(m_pTrayMenump3, SIGNAL(sigpausemp3()), this, SLOT(pauseWindowmp3()));
    connect(m_pTrayMenump3, SIGNAL(signextmp3()), this, SLOT(nextWindowmp3()));
    connect(m_pTrayMenump3, SIGNAL(sigpremp3()), this, SLOT(preWindowmp3()));


    //运行播放器，添加列表
    qplayer = new QMediaPlayer;
    qPlayerList = new QMediaPlaylist;
    //方法一 添加一个默认音源，防止段错误
    //官方解释setMedia 函数会立即返回，不会等待媒体加载完毕。崩溃的原因应该就是有时加载快，完成了播放正常，
    //有时加载慢未完成，播放就崩溃。
    ui->verticalSlider->setValue(VOL);
    qPlayerList->setCurrentIndex(1);
    qPlayerList->setPlaybackMode(QMediaPlaylist::Loop);
    qplayer->setPlaylist(qPlayerList);
    //方法二 槽函数 监听QMediaPlayer::mediaStatusChanged信号   进行连接
    connect(qplayer,&QMediaPlayer::mediaStatusChanged,this,&Widget::initStatus);

}

Widget::~Widget()
{
    delete ui;
}

void Widget::initStatus(QMediaPlayer::MediaStatus status)
{
    if(status == QMediaPlayer::EndOfMedia)
    {
        qplayer->setPosition(0);
        int row=ui->listWidget_3->currentRow();
        if(row==ui->listWidget_3->count()-1)
            row=0;
        else
            row++;
        ui->listWidget_3->setCurrentRow(row);

        on_listWidget_3_itemDoubleClicked(ui->listWidget_3->currentItem());
     }
}

void Widget::onTimerOut()
{
    ui->bar->setValue(qplayer->position()*maxValue/qplayer->duration());
}
void Widget::on_open_but_3_clicked()//打开文件
{
    QStringList fileNames = QFileDialog::getOpenFileNames(this,"文件选择","E:","mp3(*.mp3)");
    //判断reload
    if(if_reload)
    {
        delete timer;
    }
    if_reload=true;

    //加载文件
    foreach(QString const& str,fileNames){
        QUrl url(str);
        qPlayerList->addMedia(url);
    }
    ui->listWidget_3->addItems(fileNames);
    if(qPlayerList->isEmpty());
    else
        playlist=1;
}

void Widget::on_pre_but_3_clicked()//上一首
{
    if(playlist)
    {
        int row=ui->listWidget_3->currentRow();
        if(row==0)
            row=ui->listWidget_3->count();
        row--;

        ui->listWidget_3->setCurrentRow(row);
        on_listWidget_3_itemDoubleClicked(ui->listWidget_3->currentItem());
    }else{
        QMessageBox::warning(NULL,"提示","播放列表为空");
    }
}

void Widget::on_ply_but_3_clicked()//播放
{
    if(playlist){
        /*设置音量*/
        qplayer->setVolume(ui->verticalSlider->value());
         if(if_play)
         {
             delete timer;
         }
         if_play = true;
         /*播放时，将timer连接到OnTimerOUT槽函数，获取最大时间*/
         ui->bar->setRange(0,maxValue);
         timer = new QTimer();
         timer->setInterval(1000);
         timer->start();
         //将timer连接至onTimerOut
         connect(timer, SIGNAL(timeout()), this, SLOT(onTimerOut()));
         qplayer->play();
         playstat=1;
    }else{
         QMessageBox::warning(NULL,"提示","播放列表为空");
    }

}

void Widget::on_pasu_but_3_clicked()//暂停
{
    if(playstat)
        qplayer->pause();
    else
        QMessageBox::warning(NULL,"提示","暂时没有歌曲正在播放");
}

void Widget::on_next_but_3_clicked()//下一首
{
    if(playlist){
        int row=ui->listWidget_3->currentRow();
        if(row==ui->listWidget_3->count()-1)
            row=0;
        else
            row++;

        ui->listWidget_3->setCurrentRow(row);

        on_listWidget_3_itemDoubleClicked(ui->listWidget_3->currentItem());
    }else{
        QMessageBox::warning(NULL,"提示","播放列表为空");
    }
}

void Widget::on_stop_but_3_clicked()//停止播放
{
    if(playstat)
        qplayer->stop();
    else
        QMessageBox::warning(NULL,"提示","暂时没有歌曲正在播放");
}

void Widget::on_verticalSlider_sliderMoved(int position)//音量调节
{
    qplayer->setVolume(position);
}


void Widget::on_listWidget_3_itemDoubleClicked(QListWidgetItem *item)//双击播放列表事件
{
    Q_UNUSED(item);//去掉不用的宏
    int row = ui->listWidget_3->currentRow();
    qPlayerList->setCurrentIndex(row);


    qplayer->setVolume(ui->verticalSlider->value());

    on_ply_but_3_clicked();
}

void Widget::on_bar_sliderMoved(int position)//播放进度条移动
{
    if(playstat){
        timer->stop();
        qplayer->setPosition(position*qplayer->duration()/maxValue);
    }else{
         QMessageBox::warning(NULL,"提示","暂时没有歌曲正在播放");
    }
}

void Widget::on_bar_sliderPressed()//选中播放进度条
{
    if(playstat)
        qplayer->setPosition(ui->bar->value()*qplayer->duration()/maxValue);
    else
        QMessageBox::warning(NULL,"提示","暂时没有歌曲正在播放");
}

void Widget::on_bar_sliderReleased()//松开播放进度条
{
    timer->start();
}

void Widget::on_closebt_clicked()
{
    this->close();
}

void Widget::on_smbt_clicked()
{
     this->showMinimized();
}


void Widget::showWindowmp3()
{
    this->showNormal();
}
void Widget::smallWindowmp3()
{
    this->showMinimized();
}
void Widget::aboutWindowmp3()
{
    QMessageBox::information(NULL,"关于","Copyright@夏启阳");
}

void Widget::playWindowmp3()
{
    if(playlist)
        on_ply_but_3_clicked();
    else
        QMessageBox::warning(NULL,"提示","播放列表为空");
}

void Widget::pauseWindowmp3()
{
    if(playstat)
        on_pasu_but_3_clicked();
    else
        QMessageBox::warning(NULL,"提示","暂时没有歌曲正在播放");
}

void Widget::nextWindowmp3()
{
    if(playlist)
        on_next_but_3_clicked();
    else
        QMessageBox::warning(NULL,"提示","播放列表为空");
}

void Widget::preWindowmp3()
{
    if(playlist)
        on_pre_but_3_clicked();
    else
        QMessageBox::warning(NULL,"提示","播放列表为空");
}

void Widget::closeWindowmp3()
{
    this->close();
}

void Widget::closeEvent(QCloseEvent *event){
    QMessageBox msgBox;
    int ret = msgBox.warning(this, "别呀", "真的不听歌了嘛?",
                   QMessageBox::Yes | QMessageBox::Cancel,
                   QMessageBox::Yes);
    switch (ret) {
    case QMessageBox::Yes:
        event->accept();
        break;
    case QMessageBox::Cancel:
        event->ignore();
        break;
    default:
        break;
    }
}

