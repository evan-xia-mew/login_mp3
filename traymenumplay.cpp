#include "traymenumplay.h"
#include "widget.h"


TrayMenuMplay::TrayMenuMplay()
{
    initActionsmp3();

    connect(m_pShowActionmp3,SIGNAL(triggered(bool)),this,SIGNAL(sigshowmp3()));
    connect(m_pAboutActionmp3,SIGNAL(triggered(bool)),this,SIGNAL(sigaboutmp3()));
    connect(m_pQuitActionmp3,SIGNAL(triggered(bool)),this,SIGNAL(sigclowmp3()));
    connect(m_pSmallActionmp3,SIGNAL(triggered(bool)),this,SIGNAL(sigsmallmp3()));

    connect(m_playActionmp3,SIGNAL(triggered(bool)),this,SIGNAL(sigplaymp3()));
    connect(m_pauseActionmp3,SIGNAL(triggered(bool)),this,SIGNAL(sigpausemp3()));
    connect(m_nextActionmp3,SIGNAL(triggered(bool)),this,SIGNAL(signextmp3()));
    connect(m_preActionmp3,SIGNAL(triggered(bool)),this,SIGNAL(sigpremp3()));
}
TrayMenuMplay::~TrayMenuMplay()
{
}

void TrayMenuMplay::initActionsmp3()
{
    m_pShowActionmp3 = new QAction(QIcon(":/img/1.jpg"),"显示窗口");
    m_pSmallActionmp3 = new QAction(QIcon(":/img/8.jpg"),"窗口最小化");
    m_pAboutActionmp3 = new QAction(QIcon(":/img/2.jpg"),"关于作者");
    m_pQuitActionmp3 = new QAction(QIcon(":/img/6.jpg"),"退出程序");
    m_playActionmp3 = new QAction(QIcon(":/img/3.jpg"),"播放音乐");
    m_pauseActionmp3 = new QAction(QIcon(":/img/4.jpg"),"暂停音乐");
    m_preActionmp3 = new QAction(QIcon(":/img/7.jpg"),"上一首");
    m_nextActionmp3 = new QAction(QIcon(":/img/5.jpg"),"下一首");

    addAction(m_pShowActionmp3);
    addAction(m_pSmallActionmp3);
    addAction(m_pAboutActionmp3);
    addSeparator();
    addAction(m_playActionmp3);
    addAction(m_pauseActionmp3);
    addAction(m_nextActionmp3);
    addAction(m_preActionmp3);

    addSeparator();
    addAction(m_pQuitActionmp3);
}
