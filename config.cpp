#include "config.h"

Config::Config(const QString &path)
{
    m_pSettings = new QSettings(path,QSettings::IniFormat);
}

Config::~Config()
{
    delete m_pSettings;
    m_pSettings=nullptr;
}

void Config::writeSetting(QString strNode, QString key, QVariant qvar)
{
    m_pSettings->setValue(QString("%1/%2").arg(strNode).arg(key),qvar);
}

QVariant Config::readSetting(QString strNode, QString key)
{
    return m_pSettings->value(QString("%1/%2").arg(strNode).arg(key));
}

