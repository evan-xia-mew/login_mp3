#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QMessageBox>
#include <QTimer>
#include "config.h"
#include "widget.h"


namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();

signals:
    void sendmsg(int);

private slots:
    void on_pb_clicked();
    void setpass(bool flag);
    void slot_rember_PWD(int index);
    void auto_set_PWD();
private:
    Ui::Dialog *ui;

    Config *m_pConfig;
    QString m_username;
    QString m_pwd;
    QMessageBox *qmg;
    QTimer *myTimer;

    QString m_RememberName;
    QString m_RememberPWD;
    bool m_FlagRememberPWD;

    Widget *m_mp3_widget;


};

#endif // DIALOG_H
