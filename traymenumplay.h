#ifndef TRAYMENUMPLAY_H
#define TRAYMENUMPLAY_H

#include <QMenu>
#include <QAction>

class TrayMenuMplay : public QMenu
{
    Q_OBJECT
public:
    TrayMenuMplay();
    ~TrayMenuMplay();

signals:
    void sigshowmp3();
    void sigaboutmp3();
    void sigclowmp3();
    void sigsmallmp3();

    void sigplaymp3();
    void sigpausemp3();
    void signextmp3();
    void sigpremp3();

private:
    void initActionsmp3();

private:
    QAction *m_pShowActionmp3;
    QAction *m_pSmallActionmp3;
    QAction *m_pAboutActionmp3;
    QAction *m_pQuitActionmp3;
    QAction *m_playActionmp3;
    QAction *m_pauseActionmp3;
    QAction *m_nextActionmp3;
    QAction *m_preActionmp3;
};

#endif // TRAYMENUMPLAY_H
