#ifndef TRAYMENU_H
#define TRAYMENU_H

#include <QMenu>
#include <QAction>
//托盘
class TrayMenu : public QMenu
{
     Q_OBJECT
public:
    TrayMenu();
    ~TrayMenu();

signals:
    void sigshow();
    void sigabout();
    void sigclow();
    void sigsmall();


private:
    void initActions();

private:
    QAction *m_pShowAction;
    QAction *m_pSmallAction;
    QAction *m_pAboutAction;
    QAction *m_pQuitAction;

};

#endif // TRAYMENU_H
