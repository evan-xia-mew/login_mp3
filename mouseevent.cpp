#include "mouseevent.h"

mouseevent::mouseevent(QWidget *parent) :
    QWidget(parent)
{
    m_bFlag=false;
}

mouseevent::~mouseevent(){}

void mouseevent::mousePressEvent(QMouseEvent *e)
{
    Q_UNUSED(e);//去掉不用的宏
    if(e->button() == Qt::LeftButton)
    {
        m_bFlag=true;
        m_point = e->globalPos() - pos();
    }
}

void mouseevent::mouseReleaseEvent(QMouseEvent *e)
{
    Q_UNUSED(e);//去掉不用的宏
    if(e->button() == Qt::LeftButton)
    {
        m_bFlag=false;
    }
}

void mouseevent::mouseMoveEvent(QMouseEvent *e)
{
    Q_UNUSED(e);//去掉不用的宏
    if(e->buttons() & Qt::LeftButton)
    {
        move(e->globalPos() - m_point);
    }
}
