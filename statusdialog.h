#ifndef STATUSDIALOG_H
#define STATUSDIALOG_H

#include <QDialog>

namespace Ui {
class statusDialog;
}

class statusDialog : public QDialog
{
    Q_OBJECT

public:
    explicit statusDialog(QWidget *parent = nullptr);
    ~statusDialog();

private:
    Ui::statusDialog *ui;
};

#endif // STATUSDIALOG_H
